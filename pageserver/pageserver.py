"""
  A trivial web server in Python.

  Based largely on https://docs.python.org/3.4/howto/sockets.html
  This trivial implementation is not robust:  We have omitted decent
  error handling and many other things to keep the illustration as simple
  as possible.

  FIXME:
  Currently this program always serves an ascii graphic of a cat.
  Change it to serve files if they end with .html or .css, and are
  located in ./pages  (where '.' is the directory from which this
  program is run).
"""

import config    # Configure from .ini files and command line
import logging   # Better than print statements
import os #temporarily use this to check
from pathlib import Path # So we can easily check if files exist
logging.basicConfig(format='%(levelname)s:%(message)s',
                    level=logging.INFO)
log = logging.getLogger(__name__)
# Logging level may be overridden by configuration 

import socket    # Basic TCP/IP communication on the internet
import _thread   # Response computation runs concurrently with main program


def listen(portnum):
    """
    Create and listen to a server socket.
    Args:
       portnum: Integer in range 1024-65535; temporary use ports
           should be in range 49152-65535.
    Returns:
       A server socket, unless connection fails (e.g., because
       the port is already in use).
    """
    # Internet, streaming socket
    serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # Bind to port and make accessible from anywhere that has our IP address
    serversocket.bind(('', portnum))
    serversocket.listen(1)    # A real server would have multiple listeners
    return serversocket


def serve(sock, func):
    """
    Respond to connections on sock.
    Args:
       sock:  A server socket, already listening on some port.
       func:  a function that takes a client socket and does something with it
    Returns: nothing
    Effects:
        For each connection, func is called on a client socket connected
        to the connected client, running concurrently in its own thread.
    """
    while True:
        log.info("Attempting to accept a connection on {}".format(sock))
        (clientsocket, address) = sock.accept()
        _thread.start_new_thread(func, (clientsocket,))


##
# Starter version only serves cat pictures. In fact, only a
# particular cat picture.  This one.
##
CAT = """
     ^ ^
   =(   )=
"""

# HTTP response codes, as the strings we will actually send.
# See:  https://en.wikipedia.org/wiki/List_of_HTTP_status_codes
# or    http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html
##
STATUS_OK = "HTTP/1.0 200 OK\n\n"
STATUS_FORBIDDEN = "HTTP/1.0 403 Forbidden\n\n"
STATUS_NOT_FOUND = "HTTP/1.0 404 Not Found\n\n"
STATUS_NOT_IMPLEMENTED = "HTTP/1.0 401 Not Implemented\n\n"


def respond(sock):
    """
    This server responds only to GET requests (not PUT, POST, or UPDATE).
    Any valid GET request is answered with an ascii graphic of a cat.
    """
    sent = 0
    request = sock.recv(1024)  # We accept only short requests
    request = str(request, encoding='utf-8', errors='strict')
    log.info("--- Received request ----")
    log.info("Request was {}\n***\n".format(request))

    parts = request.split()
    if len(parts) > 1 and parts[0] == "GET":
        transmit(STATUS_OK, sock)
        transmitMe = CAT  #Using transmitMe to get whats going to be transmitted, then transmit it after the logic
        checkForSlashes = parts[1]  #parts[1] gives us the entire path/of/the/page, which we'll be scanning for the // forbidden case later
        options = get_options()  #We need this to access DOCROOT so we can make a nice path
        pathToPage = Path(str(Path.cwd()) + options.DOCROOT + parts[1]) #This lets us run the server from any folder on the machine and still have a nice path, since the working directory and DOCROOT are implied
        checkForbid = parts[1].split('/')  #To check for forbidden path we'll need the part after the last '/' in the path
        checkFirst = checkForbid.pop()  #So once the path is split into parts, the last one is stored in checkFirst
        if (len(checkFirst) > 1): #Ensuring the final part of path is long enough we wont get string out of bounds
            if (checkFirst[0] == '.' and checkFirst[1] == '.'): #If the first two characters of the page are '..' then
                transmitMe = STATUS_FORBIDDEN                   #We send a forbidden status
        if (len(checkFirst) >= 1): #Ensuring the final part of the path is long enough we wont get string out of bounds
            if (checkFirst[0] == '~'):         #If the first character of the page is '~' then
                transmitMe = STATUS_FORBIDDEN  #We send a forbidden status
            elif (checkForSlashes.find('//') != -1): #Search for '//' in the path/to/page, which is forbidden, and if we find it
                transmitMe = STATUS_FORBIDDEN  #We send a forbidden status
        if (transmitMe != STATUS_FORBIDDEN and pathToPage.is_file()):  #If the path is a path to a valid file and we haven't already judged it to be forbidden, we can send the file
            with open(pathToPage) as readThisFile:  #If it's valid, we open the file
                transmitMe = readThisFile.read()  #And prepare it for transmission
        else:                                     #Otherwise, we'll send an error
            if (transmitMe != STATUS_FORBIDDEN):  #We only need to send not found status if it's not forbidden
                transmitMe = STATUS_NOT_FOUND  #If it's not forbidden, and it's not found, we send a not found status
        transmit(transmitMe, sock)  #And this is where we finally transmit, be it error or the
    else:
        log.info("Unhandled request: {}".format(request))
        transmit(STATUS_NOT_IMPLEMENTED, sock)
        transmit("\nI don't handle this request: {}\n".format(request), sock)

    sock.shutdown(socket.SHUT_RDWR)
    sock.close()
    return


def transmit(msg, sock):
    """It might take several sends to get the whole message out"""
    sent = 0
    while sent < len(msg):
        buff = bytes(msg[sent:], encoding="utf-8")
        sent += sock.send(buff)

###
#
# Run from command line
#
###


def get_options():
    """
    Options from command line or configuration file.
    Returns namespace object with option value for port
    """
    # Defaults from configuration files;
    #   on conflict, the last value read has precedence
    options = config.configuration()
    # We want: PORT, DOCROOT, possibly LOGGING

    if options.PORT <= 1000:
        log.warning(("Port {} selected. " +
                         " Ports 0..1000 are reserved \n" +
                         "by the operating system").format(options.port))

    return options


def main():
    options = get_options()
    port = options.PORT
    if options.DEBUG:
        log.setLevel(logging.DEBUG)
    sock = listen(port)
    log.info("Listening on port {}".format(port))
    log.info("Socket is {}".format(sock))
    serve(sock, respond)


if __name__ == "__main__":
    main()
