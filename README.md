# README #
Author: Andrew Evelhoch 
Contact: aevelhoc@uoregon.edu 
Description:
As stated, the goals of the project are:
  * Experience with GIT workflow with separate configuration: Fork the project, make and test changes locally, commit; turn in configuration file with reference to repo. (Project 0 is practice for this part.) 
  * Extend a tiny web server in Python, to check understanding of basic web architecture
  * Use automated tests to check progress (plus manual tests for good measure)

While doing this project, I did all three. Well, I'm writing this readme before committing the changes back to my repo and turning in the ini, but doing it in that order would be weird. In addition to completing the goals of the project, I learned a bit about coding in python, despite having zero experience before. I added the features for pyserver to check if a file exists, serve it up if it does, and send corresponding errors if it doesn't. This was a lesson in some of the string operations python has available and a little about paths. I also set up a linux virtual machine and used the bash terminal extensively for both getting the program running and for doing the tests on it. 

The functionality of the server if that once you run it in your terminal, you can access files that are in the base folder of the program (where the makefile is) + whatever path was specified as DOCROOT in the credentials.ini. This is done by accessing localhost on the port also specified in DOCROOT and then the path to the file from there. If a request is bad, it will serve up a corresponding error, a 404 for not found or a 403 if the request is trying to do something possibly untoward.
